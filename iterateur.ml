(* 1*)
let affiche_liste_entiers l =
    List.iter ( fun x -> Printf.printf "%d-" x ) l
;;

let count n l =
    List.fold_left (fun acc x-> if x = n then acc + 1 else acc + 0 ) 0 l
;;

let flatten l =
    List.fold_right ( fun x acc -> x @ acc )  l []
;;

let fst_list l =
    List.fold_right ( fun (x,y) acc -> x :: acc ) l []
;;

let fst_list_map l =
    List.map fst l
;;

(* 2 *)
let couperV1 l =
    let rec aux l1 (l2,l3) =
        match l1 with
        | [] -> (l2,l3)
        | x :: [] -> (x :: l2,l3)
        | x1 :: x2 :: s -> aux s (x1 :: l2, x2 :: l3)
    in
    aux l ([],[])
;;


let couperV2 l =
    let rec aux l1 (l2,l3) b =
        match l1 with
        | [] -> (l2,l3)
        | x :: s -> if b then aux s (x :: l2,l3) (not b) else aux s (l2, x :: l3) (not b)
    in
    aux l ([],[]) true
;;

let couperV3 l =
    fst (List.fold_left ( fun ((l2,l3),b) x -> if b then ((x::l2,l3),(not b)) else ((l2,x::l3),(not b))) (([],[]),true) l )
;;

(* fix me *)
let rec fusion_v0 comp l1 l2 =
    match l1,l2 with
    | [],_ -> l2
    | _,[] -> l1 (* useless *)
    | x1::s1,x2::s2 -> 
            if ( comp x1 x2 < 0 ) then
                fusion_v0 comp s1 ( List.rev (x1 :: l2))
            else
                fusion_v0 comp l1 ( List.rev (x2 :: s2))
;;

let fusion_v1 comp l1 l2 =
    let rec aux acc l1 l2 =
        match l1,l2 with
        | [], _ -> List.rev ( List.rev (l2) @ acc )
        | _, [] -> List.rev ( List.rev (l1) @ acc ) 
        | x1::s1,x2::s2 ->
            if ( (comp x1 x2) < 0) then
                aux (x1 :: acc) s1 l2
            else
                aux (x2 :: acc) l1 s2
    in
    aux [] l1 l2
;;

(* version prof *)
let merge compare l1 l2 =
    let rec aux acc l1 l2 =
        match l1, l2 with
        | [], _ -> List.rev (List.rev_append l2 acc)
        | _, [] -> List.rev (List.rev_append l1 acc)
        | x :: l1, y :: l2 ->
                if (compare x y) < 0 then aux (x :: acc) l1 l2
                else aux ( y :: acc ) l1 l2
    in
    aux [] l1 l2
;;

let rec trier comp l =
    match l with 
    | [] -> []
    | x :: [] -> [x]
    | _ -> 
        let l1,l2 = couperV1 l in
        let sorted_l1 = trier comp l1 in
        let sorted_l2 = trier comp l2 in
        fusion_v1 comp sorted_l1 sorted_l2
;;

let make_list n =
    Random.self_init();
    let rec aux acc i n =
        match i with
        | 0 -> acc
        | _ -> 
            let m = Random.int (n-1) in
            aux ( m :: acc ) (i-1) n
    in
    aux [] n n
;;

let () =
    let l = make_list 100 in
    affiche_liste_entiers (trier (fun a b -> if a < b then -1 else (if a > b then 1 else 0)) l)
;;
